package app.commands;

import app.Player;
import app.ui.Console;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class AddPlayerCommandTest {

    private Console console;
    private final ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setupConsole() {
        console = new Console(System.in, new PrintStream(output));
    }

    @Test
    public void addPlayerThenPlayerAdded() {

        List<Player> players = new ArrayList<>();
        players.add(new Player("mario"));
        String newPlayer = "fabio";

        new AddPlayerCommand(console, players, newPlayer).execute();

        Assert.assertEquals(2, players.size());
        Assert.assertEquals("players: mario, fabio\n", output.toString());
    }

    @Test
    public void addDuplicatedPlayerThenPlayerNotAdded() {

        List<Player> players = new ArrayList<>();
        String playerName = "fabio";
        players.add(new Player(playerName));

        new AddPlayerCommand(console, players, playerName).execute();

        Assert.assertEquals(1, players.size());
        Assert.assertEquals(playerName + ": already existing player\n", output.toString());
    }

    @Test
    public void addPlayerThenNotAddedMaxPlayerReached() {

        List<Player> players = new ArrayList<>();
        players.add(new Player("1"));
        players.add(new Player("2"));
        players.add(new Player("3"));
        players.add(new Player("4"));
        players.add(new Player("5"));
        players.add(new Player("6"));

        String newPlayer = "7";

        new AddPlayerCommand(console, players, newPlayer).execute();

        Assert.assertEquals(6, players.size());
    }

}