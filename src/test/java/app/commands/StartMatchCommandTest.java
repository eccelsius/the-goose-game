package app.commands;

import app.MatchStatus;
import app.Player;
import app.config.GameParams;
import app.ui.Console;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class StartMatchCommandTest {

    private Console console;
    private final ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setupConsole() {
        console = new Console(System.in, new PrintStream(output));
    }

    @Test
    public void startThenMatchStarts() {

        List<Player> players = new ArrayList<>();
        players.add(new Player("1"));
        players.add(new Player("2"));
        MatchStatus status = new MatchStatus();

        new StartMatchCommand(console, players, status).execute();

        Assert.assertTrue(status.isStarted());
    }

    @Test
    public void startThenMatchNotStartsMinPlayerNotReached() {

        List<Player> players = new ArrayList<>();
        players.add(new Player("1"));
        MatchStatus status = new MatchStatus();

        new StartMatchCommand(console, players, status).execute();

        Assert.assertFalse(status.isStarted());
        Assert.assertEquals("Can't start a match until minimum number of player (" + GameParams.MIN_PLAYERS + ") is not reached. Please add more players.\n", output.toString());

    }
}