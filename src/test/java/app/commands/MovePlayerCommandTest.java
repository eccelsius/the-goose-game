package app.commands;

import app.Board;
import app.MatchStatus;
import app.Player;
import app.cells.StartCell;
import app.config.GameParams;
import app.ui.Console;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class MovePlayerCommandTest {

    private Console console;
    private final ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setupConsole() {
        console = new Console(System.in, new PrintStream(output));
    }

    @Test
    public void moveAutomaticallyTest() {

        MatchStatus matchStatus = initializeMatchStatus();
        Board board = initializeBoard();

        new MovePlayerCommand(console, matchStatus, board, matchStatus.getPlayingPlayer().getName(), new ArrayList<>()).execute();

        Assert.assertNotEquals(new StartCell(), matchStatus.getPlayingPlayer().getCurrentCell());

    }

    @Test
    public void moveManuallyTest() {

        MatchStatus matchStatus = initializeMatchStatus();
        Board board = initializeBoard();

        new MovePlayerCommand(console, matchStatus, board, matchStatus.getPlayingPlayer().getName(), List.of(2, 2)).execute();

        Assert.assertEquals(board.getCellByNumber(4), matchStatus.getPlayingPlayer().getCurrentCell());

    }

    @Test
    public void moveOnBridgeThenJump() {

        MatchStatus matchStatus = initializeMatchStatus();
        Board board = initializeBoard();

        new MovePlayerCommand(console, matchStatus, board, matchStatus.getPlayingPlayer().getName(), List.of(2, 4)).execute();

        Assert.assertEquals(board.getCellByNumber(GameParams.BRIDGE_LANDING_NUMBER), matchStatus.getPlayingPlayer().getCurrentCell());

    }

    @Test
    public void moveOnGooseThenMoveAgain() {

        MatchStatus matchStatus = initializeMatchStatus();
        Board board = initializeBoard();

        new MovePlayerCommand(console, matchStatus, board, matchStatus.getPlayingPlayer().getName(), List.of(1, 4)).execute();

        Assert.assertEquals(board.getCellByNumber(10), matchStatus.getPlayingPlayer().getCurrentCell());

    }

    @Test
    public void moveOnGooseMultipleTimesThenMoveMultipleTimes() {

        MatchStatus matchStatus = initializeMatchStatus();
        Board board = initializeBoard();

        new MovePlayerCommand(console, matchStatus, board, matchStatus.getPlayingPlayer().getName(), List.of(5, 4)).execute();

        Assert.assertEquals(board.getCellByNumber(36), matchStatus.getPlayingPlayer().getCurrentCell());

    }

    @Test
    public void moveOverMaxCellThenBounce() {

        Board board = initializeBoard();
        Player player = new Player("fabio");
        player.setCurrentCell(board.getCellByNumber(GameParams.WINNING_CELL_NUMBER - 1));
        MatchStatus matchStatus = initializeMatchStatus(List.of(player));

        new MovePlayerCommand(console, matchStatus, board, matchStatus.getPlayingPlayer().getName(), List.of(1, 1)).execute();

        Assert.assertEquals(board.getCellByNumber(GameParams.WINNING_CELL_NUMBER -1), matchStatus.getPlayingPlayer().getCurrentCell());
        Assert.assertFalse(matchStatus.isFinished());

    }

    @Test
    public void moveOnWinningCellThenMatchFinish() {

        Board board = initializeBoard();
        Player player = new Player("fabio");
        player.setCurrentCell(board.getCellByNumber(GameParams.WINNING_CELL_NUMBER - 2));
        MatchStatus matchStatus = initializeMatchStatus(List.of(player));

        new MovePlayerCommand(console, matchStatus, board, matchStatus.getPlayingPlayer().getName(), List.of(1, 1)).execute();

        Assert.assertEquals(board.getWinningCell(), matchStatus.getPlayingPlayer().getCurrentCell());
        Assert.assertTrue(matchStatus.isFinished());

    }

    @Test
    public void moveOnCellWithOtherPlayersThenPrank() {

        Board board = initializeBoard();
        Player player1 = new Player("1");
        Player player2 = new Player("2");
        Player player3 = new Player("3");

        player1.setCurrentCell(board.getCellByNumber(8));
        player1.setPreviousCell(board.getCellByNumber(0));

        player2.setCurrentCell(board.getCellByNumber(10));
        player2.setPreviousCell(board.getCellByNumber(4));

        player3.setCurrentCell(board.getCellByNumber(10));
        player3.setPreviousCell(board.getCellByNumber(2));

        MatchStatus matchStatus = initializeMatchStatus(List.of(player1, player2, player3));

        new MovePlayerCommand(console, matchStatus, board, matchStatus.getPlayingPlayer().getName(), List.of(1, 1)).execute();

        Assert.assertEquals(board.getCellByNumber(10), player1.getCurrentCell());
        Assert.assertEquals(board.getCellByNumber(8), player1.getPreviousCell());

        Assert.assertEquals(board.getCellByNumber(8), player2.getCurrentCell());
        Assert.assertEquals(board.getCellByNumber(8), player2.getPreviousCell());

        Assert.assertEquals(board.getCellByNumber(8), player3.getCurrentCell());
        Assert.assertEquals(board.getCellByNumber(8), player3.getPreviousCell());
    }

    private MatchStatus initializeMatchStatus() {
        Player player = new Player("fabio");
        return initializeMatchStatus(List.of(player));
    }

    private MatchStatus initializeMatchStatus(List<Player> players) {
        MatchStatus matchStatus = new MatchStatus();
        matchStatus.setPlayers(players);
        return matchStatus;
    }

    private Board initializeBoard() {
        Board board = new Board();
        board.initialize();
        return board;
    }

}