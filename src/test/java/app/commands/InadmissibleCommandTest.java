package app.commands;

import app.ui.Console;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class InadmissibleCommandTest {

    private Console console;
    private final ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setupConsole() {
        console = new Console(System.in, new PrintStream(output));
    }

    @Test
    public void inadmissibleCommandThenDisplayMessage() {

        String inadmissibleInput = "this is an inadmissible input";

        new InadmissibleCommand(console, inadmissibleInput).execute();

        String expectedMessage = new StringBuilder()
                .append("<" + inadmissibleInput + "> is not an admissible command... Admissible commands are e.g. : \n")
                .append("add Player playerName (BEFORE MATCH STARTS)\n")
                .append("start (BEFORE MATCH STARTS)\n")
                .append("move player playerName (AFTER MATCH STARTS)\n")
                .append("move player playerName 2, 6 (AFTER MATCH STARTS)\n").toString();

        Assert.assertEquals(expectedMessage, output.toString());
    }
}