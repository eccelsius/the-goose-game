package app.commands;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CommandParserTest {

    @Test
    public void getPlayerNameTest() {

        String playerName = "fabio";
        String addInput = "add player " + playerName;
        String moveAutoInput = "move player " + playerName;
        String moveManualInput = "move player " + playerName + " 2, 3";
        String invalidInput = "this is invalid input";

        String actual1 = new CommandParser(addInput).getPlayerName();
        String actual2 = new CommandParser(moveAutoInput).getPlayerName();
        String actual3 = new CommandParser(moveManualInput).getPlayerName();
        String actual4 = new CommandParser(invalidInput).getPlayerName();

        Assert.assertEquals(actual1, playerName);
        Assert.assertEquals(actual2, playerName);
        Assert.assertEquals(actual3, playerName);
        Assert.assertNull(actual4);
    }

    @Test
    public void getDicesValuesTest() {

        String validInput = "move player fabio 2, 3";
        String invalidInputNotDiceValues = "move player fabio 7, 0";
        String invalidInput = "move player fabio a, b";

        List<Integer> actual1 = new CommandParser(validInput).getDicesValues();
        List<Integer> actual2 = new CommandParser(invalidInputNotDiceValues).getDicesValues();
        List<Integer> actual3 = new CommandParser(invalidInput).getDicesValues();

        Assert.assertEquals(actual1, List.of(2, 3));
        Assert.assertTrue(actual2.isEmpty());
        Assert.assertTrue(actual3.isEmpty());

    }


}