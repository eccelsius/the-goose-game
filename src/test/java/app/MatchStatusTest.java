package app;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class MatchStatusTest {

    @Test
    public void nextTurnThenIncrementTurn() {

        MatchStatus matchStatus = new MatchStatus();
        Player player1 = new Player("1");
        Player player2 = new Player("2");
        List<Player> players = List.of(player1, player2);
        matchStatus.setPlayers(players);

        matchStatus.nextTurn();

        Assert.assertEquals(player2, matchStatus.getPlayingPlayer());
        Assert.assertFalse(matchStatus.isFinished());

    }

    @Test
    public void nextTurnAfterWinThenMatchFinish() {

        MatchStatus matchStatus = new MatchStatus();
        Player player1 = new Player("1");
        Board board = new Board();
        board.initialize();
        player1.setCurrentCell(board.getWinningCell());
        Player player2 = new Player("2");
        List<Player> players = List.of(player1, player2);
        matchStatus.setPlayers(players);

        matchStatus.nextTurn();

        Assert.assertEquals(player1, matchStatus.getPlayingPlayer());
        Assert.assertTrue(matchStatus.isFinished());

    }

}