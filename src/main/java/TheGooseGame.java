import app.Match;
import app.ui.Console;

public class TheGooseGame {

    public static void main(String[] args) {

        Console console = new Console();
        Match match = new Match(console);

        while (!match.isFinished()) {

            match.start();

        }
    }

}
