package app.cells;

public interface Cell {

    String getName();
    int getNumber();
    boolean isGoose();
    boolean isBridge();
    String toString();
}
