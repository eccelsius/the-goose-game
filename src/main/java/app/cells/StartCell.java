package app.cells;

import java.util.Objects;

public class StartCell implements Cell {

    private int number;
    private String name;

    public StartCell() {
        this.number = 0;
        this.name = "Start";
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getNumber() {
        return this.number;
    }

    @Override
    public boolean isGoose() {
        return false;
    }

    @Override
    public boolean isBridge() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StartCell startCell = (StartCell) o;
        return number == startCell.number &&
                name.equals(startCell.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, name);
    }

    @Override
    public String toString() {
        return "StartCell{" +
                "number=" + number +
                ", name='" + name + '\'' +
                '}';
    }
}
