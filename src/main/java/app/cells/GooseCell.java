package app.cells;

import java.util.Objects;

public class GooseCell implements Cell {

    private int number;
    private String name;

    public GooseCell(int number) {
        this.number = number;
        this.name = "The Goose";
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getNumber() {
        return this.number;
    }

    @Override
    public boolean isGoose() {
        return true;
    }

    @Override
    public boolean isBridge() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GooseCell gooseCell = (GooseCell) o;
        return number == gooseCell.number &&
                name.equals(gooseCell.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, name);
    }

    @Override
    public String toString() {
        return "GooseCell{" +
                "number=" + number +
                ", name='" + name + '\'' +
                '}';
    }
}
