package app.cells;

import java.util.Objects;

public class NormalCell implements app.cells.Cell {

    private int number;
    private String name;

    public NormalCell(int number) {
        this.number = number;
        this.name = Integer.toString(number);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getNumber() {
        return this.number;
    }

    @Override
    public boolean isGoose() {
        return false;
    }

    @Override
    public boolean isBridge() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NormalCell that = (NormalCell) o;
        return number == that.number &&
                name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, name);
    }

    @Override
    public String toString() {
        return "NormalCell{" +
                "number=" + number +
                ", name='" + name + '\'' +
                '}';
    }
}
