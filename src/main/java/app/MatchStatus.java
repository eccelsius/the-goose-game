package app;

import app.config.GameParams;

import java.util.List;

public class MatchStatus {

    private boolean started;
    private boolean finished;
    private List<Player> players;
    private int turn;

    public MatchStatus() {
        this.started = false;
        this.finished = false;
        this.turn = 0;
    }

    public boolean isStarted() {
        return started;
    }

    public boolean isFinished() {
        return finished;
    }

    public void start() {
        this.started = true;
    }

    public void finish() {
        this.finished = true;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void nextTurn() {
        if (getPlayingPlayer().getCurrentCell().getNumber() == GameParams.WINNING_CELL_NUMBER) {
            finish();
        } else {
            this.turn ++;
        }
    }

    public int getTurn() {
        return turn;
    }

    public Player getPlayingPlayer() {
        return this.players.get(this.turn % this.players.size());
    }

    public List<Player> getPlayers() {
        return players;
    }
}
