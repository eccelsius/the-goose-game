package app.config;

import java.util.List;

public class GameParams {

    public static final int MIN_PLAYERS = 2;
    public static final int MAX_PLAYERS = 6;

    public static final int BRIDGE_LANDING_NUMBER = 12;
    public static final int WINNING_CELL_NUMBER = 63;
    public static final int START_CELL_NUMBER = 0;
    public static final List<Integer> GOOSE_CELLS = List.of(5, 9, 14, 18, 23, 27);
    public static final List<Integer> BRIDGE_CELLS = List.of(6);

}
