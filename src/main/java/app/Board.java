package app;

import app.cells.*;
import app.config.GameParams;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private List<Cell> cells;

    public Board() {
        cells = new ArrayList<>();
    }

    public void initialize() {

        for (int i = 0; i <= GameParams.WINNING_CELL_NUMBER; i++) {

            if (i == 0) {
                cells.add(new StartCell());
            } else if (GameParams.BRIDGE_CELLS.contains(i)) {
                cells.add(new BridgeCell(i));
            } else if (GameParams.GOOSE_CELLS.contains(i)) {
                cells.add(new GooseCell(i));
            } else {
                cells.add(new NormalCell(i));
            }
            
        }
    }

    public Cell computeToCell(Cell from, List<Integer> dicesResult) {

        int diceSum = dicesResult.stream().reduce(0, Integer::sum);
        int fromNum = from.getNumber();
        final int toNumber;

        if (fromNum + diceSum > GameParams.WINNING_CELL_NUMBER) {
            toNumber = GameParams.WINNING_CELL_NUMBER - (fromNum + diceSum - GameParams.WINNING_CELL_NUMBER);
        } else {
            toNumber = fromNum + diceSum;
        }

        return cells.stream().filter(cell -> cell.getNumber() == toNumber).findFirst().orElse(null);

    }

    public Cell getBridgeLandingCell() {
        return cells.stream().filter(cell -> cell.getNumber() == GameParams.BRIDGE_LANDING_NUMBER).findFirst().orElse(null);
    }

    public Cell getWinningCell() {
        return cells.get(cells.size() - 1);
    }

    public Cell getCellByNumber(int cellNumber) {
        return this.cells.stream()
                .filter(cell -> cell.getNumber() == cellNumber)
                .findFirst()
                .orElse(null);
    }

}
