package app;

import app.commands.*;
import app.commands.CommandParser;
import app.ui.Console;

import java.util.ArrayList;
import java.util.List;

public class Match {

    private List<Player> players;
    private Board board;
    private MatchStatus matchStatus;
    private Console console;

    public Match(Console console) {

        this.players = new ArrayList<>();
        this.board = new Board();
        this.matchStatus = new MatchStatus();
        this.console = console;
    }

    public void start() {

        this.board.initialize();

        while (!this.matchStatus.isStarted()) {

            if (this.players.isEmpty()) {
                this.console.showPlayerMenu();
            }

            String userInput = this.console.readUserInput();

            new CommandInvoker(commandFactory(userInput)).invoke();

        }

        matchStatus.setPlayers(this.players);

        while (!this.matchStatus.isFinished()) {

            int turn = matchStatus.getTurn();

            while (turn == this.matchStatus.getTurn() || this.matchStatus.isFinished()) {

                this.console.showTurnMessage(this.matchStatus.getPlayingPlayer().getName());
                String userInput = this.console.readUserInput();
                new CommandInvoker(commandFactory(userInput)).invoke();
            }

        }

    }

    public boolean isFinished() {
        return this.matchStatus.isFinished();
    }

    private Command commandFactory(String userInput) {

        CommandParser parser = new CommandParser(userInput);

        if (parser.isStartMatchCommand() && !this.matchStatus.isStarted()) {

            return new StartMatchCommand(this.console, this.players, this.matchStatus);

        } else if (parser.isAddPlayerCommand() && !this.matchStatus.isStarted()) {

            return new AddPlayerCommand(this.console, this.players, parser.getPlayerName());

        } else if (parser.isMovePlayerCommand() && this.matchStatus.isStarted()) {

            return new MovePlayerCommand(this.console, this.matchStatus, board, parser.getPlayerName(), parser.getDicesValues());
        } else {
            return new InadmissibleCommand(this.console, userInput);
        }
    }

}
