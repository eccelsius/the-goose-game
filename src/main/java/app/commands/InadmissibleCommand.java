package app.commands;

import app.ui.Console;

public class InadmissibleCommand implements Command {

    private Console console;
    private String userInput;

    public InadmissibleCommand(Console console, String userInput) {
        this.userInput = userInput;
        this.console = console;
    }

    @Override
    public void execute() {
        this.console.log("<" + userInput + "> is not an admissible command... Admissible commands are e.g. : ");
        this.console.log("add Player playerName (BEFORE MATCH STARTS)");
        this.console.log("start (BEFORE MATCH STARTS)");
        this.console.log("move player playerName (AFTER MATCH STARTS)");
        this.console.log("move player playerName 2, 6 (AFTER MATCH STARTS)");
    }
}
