package app.commands;

public class CommandInvoker {

    Command command;

    public CommandInvoker(Command command) {
        this.command = command;
    }

    public void invoke() {
        this.command.execute();
    }
}
