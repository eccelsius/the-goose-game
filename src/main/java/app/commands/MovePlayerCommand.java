package app.commands;

import app.Board;
import app.MatchStatus;
import app.Player;
import app.cells.Cell;
import app.config.GameParams;
import app.ui.Console;
import app.utils.Dice;

import java.util.List;
import java.util.stream.Collectors;

public class MovePlayerCommand implements Command {

    private Console console;
    private MatchStatus matchStatus;
    private Board board;
    private String inputPlayerName;
    private List<Integer> dicesResult;

    public MovePlayerCommand(Console console, MatchStatus matchStatus, Board board, String inputPlayerName, List<Integer> dicesResult) {
        this.console = console;
        this.matchStatus = matchStatus;
        this.board = board;
        this.inputPlayerName = inputPlayerName;
        this.dicesResult = dicesResult;
    }

    @Override
    public void execute() {


        Player playingPlayer = this.matchStatus.getPlayingPlayer();
        String message;

        if (playingPlayer.getName().equals(this.inputPlayerName)) {

            List<Integer> dicesResult = dicesResult();

            Cell toCell = board.computeToCell(playingPlayer.getCurrentCell(), dicesResult);

            if (toCell.isGoose()) {

                message = movingMessage(playingPlayer, dicesResult, toCell) + ". ";

                while (toCell.isGoose()) {

                    toCell = board.computeToCell(toCell, dicesResult);
                    message += movesAgainMessage(playingPlayer, toCell);

                    if (toCell.isGoose()) {
                        message += ", " + toCell.getName() + ". ";
                    }
                }

                playingPlayer.setPreviousCell(playingPlayer.getCurrentCell());
                playingPlayer.setCurrentCell(toCell);


            } else if (toCell.isBridge()) {

                message = movingMessage(playingPlayer, dicesResult, toCell) + ". " + jumpingMessage(playingPlayer);

                playingPlayer.setPreviousCell(playingPlayer.getCurrentCell());
                playingPlayer.setCurrentCell(this.board.getBridgeLandingCell());

            } else {
                if (playingPlayer.getCurrentCell().getNumber() >= toCell.getNumber()) {
                    message = movingMessage(playingPlayer, dicesResult, board.getWinningCell()) + ". " + bouncingMessage(playingPlayer, toCell);
                } else {
                    message = movingMessage(playingPlayer, dicesResult, toCell);
                }

                playingPlayer.setPreviousCell(playingPlayer.getCurrentCell());
                playingPlayer.setCurrentCell(toCell);
            }

            List<Player> jokedPlayer = jokedPlayers();
            prank(playingPlayer, jokedPlayer);
            message += prankMessage(playingPlayer.getCurrentCell(), playingPlayer.getPreviousCell(), jokedPlayer);

            this.matchStatus.nextTurn();
            if (this.matchStatus.isFinished()) {
                message += ". " + playingPlayer.getName() + " Wins!!";
            }

        } else {
            message = wrongTurnMessage(playingPlayer, this.inputPlayerName);
        }

        this.console.log(message);
    }

    private void prank(Player playingPlayer, List<Player> jokedPlayers) {
        jokedPlayers.forEach(player -> {
            player.setPreviousCell(playingPlayer.getPreviousCell());
            player.setCurrentCell(playingPlayer.getPreviousCell());
        });
    }

    private List<Player> jokedPlayers() {
        return this.matchStatus.getPlayers().stream()
                .filter(player -> !player.equals(this.matchStatus.getPlayingPlayer()))
                .filter(player -> player.getCurrentCell().equals(this.matchStatus.getPlayingPlayer().getCurrentCell()))
                .collect(Collectors.toList());
    }

    private List<Integer> dicesResult() {
        return this.dicesResult.isEmpty() ? List.of(new Dice().roll(), new Dice().roll()) : this.dicesResult;
    }

    private String wrongTurnMessage(Player player, String inputPlayer) {
        return "Sorry but now is the turn of " + player.getName() + " not of " + inputPlayer;
    }

    private String movingMessage(Player player, List<Integer> dicesResult, Cell landingCell) {
        return player.getName() + " rolls " + dicesResult.get(0) + ", " + dicesResult.get(1) + ". " + player.getName() + " moves from " + player.getCurrentCell().getNumber() + " to " + landingCell.getName();
    }

    private String jumpingMessage(Player player) {
        return player.getName() + " jumps to " + GameParams.BRIDGE_LANDING_NUMBER;
    }

    private String movesAgainMessage(Player player, Cell landingCell) {
        return player.getName() + " moves again and goes to " + landingCell.getNumber();
    }

    private String bouncingMessage(Player player, Cell landingCell) {
        return player.getName() + " bounces! " + player.getName() + " returns to " + landingCell.getNumber();
    }

    private String prankMessage(Cell landingCell, Cell prankCell, List<Player> jokedPlayers) {
        if (jokedPlayers.isEmpty()) {
            return "";
        }
        return ". On " + landingCell.getNumber() + " there are/is " + jokedPlayers.stream().map(Player::getName).collect(Collectors.joining(", ")) + " who return/s to " + prankCell.getNumber();
    }
}
