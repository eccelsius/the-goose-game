package app.commands;

import app.Player;
import app.config.GameParams;
import app.ui.Console;

import java.util.List;
import java.util.stream.Collectors;

public class AddPlayerCommand implements Command {

    private Console console;
    private List<Player> players;
    private String playerToAdd;

    public AddPlayerCommand(Console console, List<Player> players, String playerToAdd) {
        this.console = console;
        this.players = players;
        this.playerToAdd = playerToAdd;
    }

    @Override
    public void execute() {

        if (this.players.size() >= GameParams.MAX_PLAYERS) {
            this.console.log("Can't add more players, max number of " + GameParams.MAX_PLAYERS + " is reached.");
            return;
        }

        if (duplicatedPlayer()) {
            this.console.log(this.playerToAdd + ": already existing player");
            return;
        }

        this.players.add(new Player(this.playerToAdd));
        this.console.log("players: " + this.players.stream().map(Player::getName).collect(Collectors.joining(", ")));

    }

    private boolean duplicatedPlayer() {
        return this.players.stream()
                .map(Player::getName)
                .anyMatch(name -> name.equalsIgnoreCase(this.playerToAdd));
    }
}
