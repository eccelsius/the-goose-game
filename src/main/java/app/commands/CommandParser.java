package app.commands;

import java.util.ArrayList;
import java.util.List;

public class CommandParser {

    private static final String MOVE_PLAYER = "move player ";
    private static final String ADD_PLAYER = "add player ";
    private static final String START_MATCH = "start";
    private static final String PLAYER_NAME_PART = "([^\\s]+)";
    private static final String DICES_PART = "\\s([1-6](, [1-6]))";
    private static final String MOVE_PLAYER_AUTO = MOVE_PLAYER + PLAYER_NAME_PART;
    private static final String MOVE_PLAYER_MANUAL = MOVE_PLAYER + PLAYER_NAME_PART + DICES_PART;

    private String input;

    public CommandParser(String input) {
        this.input = input;
    }

    public String getPlayerName() {

        if (input.matches(ADD_PLAYER + PLAYER_NAME_PART)) {
            return input.replace(ADD_PLAYER, "");
        }

        if (input.matches(MOVE_PLAYER_AUTO)) {
            return input.replace(MOVE_PLAYER, "");
        }

        if (input.matches(MOVE_PLAYER_MANUAL)) {
            return input.replace(MOVE_PLAYER, "").replaceFirst(DICES_PART, "");
        }

        return null;
    }

    public List<Integer> getDicesValues() {

        List<Integer> result = new ArrayList<>();

        if (input.matches(MOVE_PLAYER_MANUAL)) {
            String[] dicesValues = this.input.replace(MOVE_PLAYER + getPlayerName(), "").replaceAll("\\s+", "").split(",");

            result.add(Integer.parseInt(dicesValues[0]));
            result.add(Integer.parseInt(dicesValues[1]));

        }

        return result;
    }

    public boolean isStartMatchCommand() {
        return input.equals(START_MATCH);
    }

    public boolean isAddPlayerCommand() {
        return input.startsWith(ADD_PLAYER);
    }

    public boolean isMovePlayerCommand() {
        return input.matches(MOVE_PLAYER_AUTO) || input.matches(MOVE_PLAYER_MANUAL);
    }

}
