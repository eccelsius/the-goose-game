package app.commands;

import app.MatchStatus;
import app.Player;
import app.config.GameParams;
import app.ui.Console;

import java.util.List;

public class StartMatchCommand implements Command {
    
    private Console console;
    private List<Player> players;
    private MatchStatus status;

    public StartMatchCommand(Console console, List<Player> players, MatchStatus status) {
        this.console = console;
        this.players = players;
        this.status = status;
    }

    @Override
    public void execute() {

        if (players.size() < GameParams.MIN_PLAYERS) {
            this.console.log("Can't start a match until minimum number of player (" + GameParams.MIN_PLAYERS + ") is not reached. Please add more players.");
        } else {
            this.status.start();
        }

    }
}
