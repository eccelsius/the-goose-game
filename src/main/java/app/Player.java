package app;

import app.cells.Cell;
import app.cells.StartCell;

import java.util.Objects;

public class Player {

    private String name;
    private Cell currentCell;
    private Cell previousCell;

    public Player(String name) {
        this.name = name;
        this.currentCell = new StartCell();
        this.previousCell = this.currentCell;
    }

    public String getName() {
        return name;
    }

    public Cell getCurrentCell() {
        return currentCell;
    }

    public Cell getPreviousCell() {
        return previousCell;
    }

    public void setCurrentCell(Cell currentCell) {
        this.currentCell = currentCell;
    }

    public void setPreviousCell(Cell previousCell) {
        this.previousCell = previousCell;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return name.equals(player.name) &&
                currentCell.equals(player.currentCell);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, currentCell);
    }
}
