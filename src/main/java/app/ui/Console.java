package app.ui;

import app.config.GameParams;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class Console {

    private Scanner scanner;
    private InputStream in;
    private PrintStream out;

    public Console(InputStream in, PrintStream out) {
        this.in = in;
        this.out = out;
        this.scanner = new Scanner(this.in);
    }

    public Console() {
        this.in = System.in;
        this.out = System.out;
        this.scanner = new Scanner(this.in);
    }

    public void showPlayerMenu() {
        this.out.println();
        this.out.println("#############################################");
        this.out.println("PLEASE ENTER " + GameParams.MIN_PLAYERS + "-" + GameParams.MAX_PLAYERS + " PLAYERS THEN ENTER \"start\"");
        this.out.println("#############################################");
        this.out.println();
    }

    public void showTurnMessage(String playerName) {
        this.out.println();
        this.out.println("########################################");
        this.out.println("IS THE TURN OF " + playerName + ", PLEASE MOVE");
        this.out.println("########################################");
        this.out.println();
    }

    public void log(String message) {
        this.out.println(message);
    }

    public String readUserInput() {
        return this.scanner.nextLine();
    }
}
